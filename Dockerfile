FROM node:14

WORKDIR /usr/src/app/

COPY package*.json ./

RUN npm install

EXPOSE 3333

CMD ["npm", "run", "dev:server"]
