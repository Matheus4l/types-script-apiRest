import { Router } from 'express';
import ProductController from '../../modules/product/controlles/ProductController';

const productRouter = Router();

productRouter.post('/', ProductController.store);

export default productRouter;
