import { Router } from 'express';
import InvoiceController from '../../modules/invoice/controlles/InvoiceController';

const invoiceRouter = Router();

invoiceRouter.post('/',InvoiceController.store);
invoiceRouter.delete('/',InvoiceController.delete);

export default invoiceRouter;
