import { Router } from 'express';
import productRouter from './product.route';
import invoiceRouter from './invoice.route';


const routes = Router();
routes.use('/product', productRouter);
routes.use('/invoice', invoiceRouter);

export default routes;
