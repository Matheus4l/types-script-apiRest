import 'reflect-metadata';
import express from 'express';
import routes from './shared/routes';
import mongo from './shared/database/index'


mongo.start()
const app = express();
app.use(express.json());
app.use(routes);

export{app}
