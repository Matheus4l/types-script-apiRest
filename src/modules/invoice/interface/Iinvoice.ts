
import mongoose from 'mongodb'
export interface Iinvoices extends mongoose.Document {
  _id:string
  cpf: string,
  product: string,
  created_at?:  Date,
  __v:number
}
