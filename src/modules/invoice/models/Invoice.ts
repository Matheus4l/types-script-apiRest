/* eslint-disable camelcase */

import{Schema, model,Document } from 'mongoose';
import { Iinvoices } from '../interface/Iinvoice';
const InvoiceSchema = new Schema({
    cpf: String,
    product: {
      type: Schema.Types.ObjectId,
      ref:"Product"
    },
    created_at: { type: Date, default: Date.now },
});

export default model< Iinvoices & Document> ('Invoice', InvoiceSchema);



