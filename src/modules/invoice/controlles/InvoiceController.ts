import { Request, Response } from 'express'
import CreateInvoiceService from '../services/CreateInvoiceService';
import DeleteInvoiceService from '../services/DeleteInvoiceService';

class InvoiceController {

  public async store(request: Request, response: Response): Promise<any> {
    const {cpf,idProduct } = request.body;
    try {
      const invoice = await CreateInvoiceService.execute({ cpf,idProduct })
      return response.json(invoice);
    } catch (err:any) {
      return response.status(400).json({ error: err.message });
    }
  }

  public async delete (request: Request, response: Response): Promise<any> {
    const {id,idProduct } = request.body;
    try {
      const invoice = await DeleteInvoiceService.execute({ id,idProduct })
      return response.json(invoice);
    } catch (err:any) {
      return response.status(400).json({ error: err.message });
    }
  }
}
export default new InvoiceController()
