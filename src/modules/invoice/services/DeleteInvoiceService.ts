import Invoice from '../models/Invoice';
import { Iinvoices } from '../interface/Iinvoice';
import UpdateProductService from '../../product/services/UpdateProductService'
import Product from '../../product/models/Product';


interface Request {
  id: string;
  idProduct: string;
}

class DeleteInvoiceService {

  public async execute({ id, idProduct }: Request): Promise<boolean|null> {

    try {
      const invoice = await Invoice.deleteOne({'_id':id});
      await UpdateProductService.deleteRelationshipsInvoices( id, idProduct );
      return true
    } catch (error:any) {
      return error
    }
  }
}

export default new DeleteInvoiceService();
