import Invoice from '../models/Invoice';
import { Iinvoices } from '../interface/Iinvoice';
import UpdateProductService from '../../product/services/UpdateProductService'
import Product from '../../product/models/Product';


interface Request {
  cpf: string;
  idProduct: string;
}

class CreateInvoiceService {
  public async execute({ cpf, idProduct }: Request): Promise<Iinvoices> {

    const invoice = await Invoice.create({ cpf, product: idProduct });
    await UpdateProductService.updateRelationshipsInvoices(invoice);

    return await invoice.populate({
      path: 'product',
      select: 'name price size description type'
    })
  }
}

export default new CreateInvoiceService();
