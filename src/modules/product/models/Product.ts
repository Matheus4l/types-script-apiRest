/* eslint-disable camelcase */
import{Schema, model, Document } from 'mongoose';
import { Iproduct } from '../interface/Iproduct';

const ProductSchema = new Schema({
    name: String,
    price:Number,
    inventory:Number,
    invoices: [{
      type: Schema.Types.ObjectId,
      ref:"Invoice"
    }],
    size: String,
    type: String,
    description: String,
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
});

export default model < Iproduct & Document >('Product', ProductSchema);


