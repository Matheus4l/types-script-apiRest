import { Request, Response } from 'express'
import CreateProductService from '../services/CreateProductService';
import GetProductService from '../services/GetProductService';
import UpdateProductService from '../services/UpdateProductService';
import { container } from 'tsyringe';



class ProductController {
  public async index(request: Request, response: Response): Promise<Response> {
    try {
      const  getProduct = container.resolve(GetProductService);
      const product = await getProduct.execute()
      return response.json(product);
    } catch (err:any) {
      return response.status(400).json({ error: err.message });
    }

  }
  public async store(request: Request, response: Response): Promise<any> {
    const { name, type, size, description, inventory,price } = request.body;
    try {
      const createProduct = container.resolve(CreateProductService);
      const product = await createProduct.execute({ name, type, size, description,inventory,price })
      return response.json(product);
    } catch (err:any) {
      return response.status(400).json({ error: err.message });
    }

  }
}

export default new ProductController()
