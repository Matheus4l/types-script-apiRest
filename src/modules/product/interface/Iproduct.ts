export interface Iproduct {
  _id?:String
  name?: String,
  price?: Number,
  invoices?:[],
  inventory?: Number,
  size?: String,
  type?: String,
  description?: String,
  created_at?:  Date,
  updated_at?:  Date
}
