
import Product from '../models/Product';
import { Iproduct } from '../interface/Iproduct';
import { Iinvoices } from '../../invoice/interface/Iinvoice';
import Invoice from '../../invoice/models/Invoice';
import{ObjectId} from 'mongodb'

interface Request {
  name: string;
  size: string;
  type: string;
  description: string;
  inventory: number;
  price: number;
}

class UpdateProductService {

  public async execute(id: ObjectId, productPayload: Iproduct): Promise<Iproduct | any> {
    try {

      console.log(productPayload)
      const product = await Product.findByIdAndUpdate(productPayload._id, { productPayload }, { new: true });

      return product;
    } catch (error: any) {
      return error;
    }
  }

  public async updateDuplicity(productPayload: Iproduct): Promise<Iproduct | any> {
    try {
      const {inventory, _id} =productPayload
      const product = await Product.findByIdAndUpdate(_id, { inventory }, { new: true });

      return product;
    } catch (error: any) {
      return error;
    }
  }
  public async updateRelationshipsInvoices(invoice: Iinvoices): Promise<Iproduct | null> {
    try {
      const product = await Product.findByIdAndUpdate(invoice.product, { $push: { invoices: invoice._id } }, { new: true });
      return product;
    } catch (error: any) {
      return error;
    }
  }

  public async deleteRelationshipsInvoices(idInvoices: string, idproduto: string): Promise<Iproduct | null> {
    try {
      const product = await Product.findByIdAndUpdate({"_id":idproduto},{ $pull: { invoices: idInvoices } } );
      return product
    } catch (error: any) {
      return error;
    }
  }
}

export default new UpdateProductService();
