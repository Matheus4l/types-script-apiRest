import { getRepository, getMongoRepository } from 'typeorm';
import { hash } from 'bcryptjs';
import Product from '../models/Product';
import { Iproduct } from '../interface/Iproduct';
import { Iinvoices } from '../../invoice/interface/Iinvoice';
import Invoice from '../../invoice/models/Invoice';

interface Request {
  name:string;
  size:string;
  type:string;
  description:string;
  inventory:number;
  price:number;
}

class GetProductService {

  public async execute():Promise<Iproduct[]|null> {
    try {
      const product = await Product.find();
      return product;
    } catch (error:any) {
      return error;
    }
  }
}

export default GetProductService;
