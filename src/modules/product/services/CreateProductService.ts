import { getRepository, getMongoRepository, getMongoManager } from 'typeorm';
import { hash } from 'bcryptjs';
import Product from '../models/Product';
import { Iproduct } from '../interface/Iproduct';
import UpdateProductService from './UpdateProductService'

interface Request {
  name: string;
  size: string;
  type: string;
  description: string;
  inventory: number;
  price: number;
}

class CreateProductService {


  public async execute({ name, size, type, description, inventory, price }: Request): Promise<Iproduct | null> {

    try {
      const ifExistProduct = await this.compareToDuplicity({ name, size, type, description, inventory, price });
      if(!ifExistProduct){
        const product = await Product.create({
          name,
          size,
          type,
          description,
          inventory,
          price
        });
        return product;
      }
      return ifExistProduct
    } catch (error:any) {
      return error;
    }
  }

  private async compareToDuplicity({ name, size, type, description, inventory, price }: Request): Promise<Iproduct|null> {
    const product = await Product.findOne({ name, type, price, size })

    if(product){
      let inventoryOld = parseFloat(`${product.inventory}`) // type Number do mongoose impede impedindo a soma direta
      product.inventory = inventory + inventoryOld
      const responseProduct = await UpdateProductService.updateDuplicity(product)
      return responseProduct
    }else{
      return null
    }

  }
}

export default CreateProductService;
