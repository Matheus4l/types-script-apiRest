# api node typescript

O Banco de Dados já está populado com informações de produto .
Será necessário  que o docker e o docker-compose estaja previamente instalado na maquina

https://docs.docker.com/engine/install/

https://docs.docker.com/compose/install/


# Inicando o Projeto com o Docker

Execute na pasta raiz e execute os seguintes comandos
```
docker-compose build --no-cache
```
```
docker-compose up -d
```

//Para testar as rodas
```
[ http://localhost:3333/product/ ]  post
```
Exemplo de payload pra teste de produto
{
	"name":"Blusa",
	"type":"Jeans",
	"size":"M",
	"inventory":3,
	"price":80,
	"description":"Blusa Jeans"
}

/////////////////////////////////////
```
[ http://localhost:3333/invoice/ ]  post / delete
 ```
Exemplo de payload pra teste de nota fiscal
{
 "cpf":"15045158707",
 "idProduct":"614cc4a60291cab1fa0834b4"
}


# Obs:
No momento da criação do produto  esta seguindo a regra de duplicidade no banco

A aplicação será levantada em http://localhost:3333



